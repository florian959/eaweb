<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 20 juin 2015

/*
	* views ~ welcome-message.php
	Vue de la class ~ welcome.class.php

*/
if(!defined("BASE_DIR")){die("403");}
?>

<h3>:smile: &nbsp;&nbsp; -lang installe_avec_succes lang-</h3>

    <br /><br />Ceci est la page par défaut, elle se trouve dans application/views/welcome-message.php

<br /><br /><hr />
Page générée en {{elapsed_time}} secondes.
<br /><br />
        <br /><br /><center><span onclick="window.location.replace('?lang=fr')">FR</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span onclick="window.location.replace('?lang=en')">EN</span></center>

        <br /><br /><br /><br />

        <!--<div class="row">
        	<div class="col col-6">Col 6</div>
        </div>  

        <div class="row">
        	<div class="col col-3">Col 3</div>
        	<div class="col col-3">Col 3</div>
        </div>  

        <div class="row">
        	<div class="col col-1">Col 1</div>
        	<div class="col col-2">Col 2</div>
        	<div class="col col-3">Col 3</div>
        </div>  -->

        <div class="row">
        	<div class="col col-lg-2 col-sm-4">Col 2</div>
        	<div class="col col-lg-8 col-sm-6">Col 4</div>
        </div>  

        <div class="row">
        	<div class="col col-lg-1 col-sm-2">Col 1</div>
        	<div class="col col-lg-9 col-sm-8">Col 5</div>
        </div>  