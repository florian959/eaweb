<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 27 juin 2015

/*
	* CONFIGURATION
	Configuration du cadriciel

*/
// Configuration du cadriciel
$config = array(
	'mode'        => 'dev',
	'timezone'    => 'Europe/Paris',
	'langages'	  => array('fr','en'),
	'template'    => $paths['master']['template'].'/main.php',



/*
	* plugins
	Gestion des plugins à charger

*/

	'plugins'       => array(
			'security' 			=> array('file' => 'security-plugin/index.php', 'Load' => 'onstart')
		),



/*
	* dossiers
	Configuration des dossiers pour la template

*/

	'css-folder'   => 'http://'.$_SERVER['HTTP_HOST'].$paths['base-url'].$paths['master']['template'].'/css',
	'js-folder'    => 'http://'.$_SERVER['HTTP_HOST'].$paths['base-url'].$paths['master']['template'].'/js',
	'short-url'    => 'http://'.$_SERVER['HTTP_HOST'].$paths['base-url']."/",



/*
	* url
	Configuration de l'url d'accés

*/

	'url' => array(
		'useIndex' => True
		),



/*
	* cache
	Gestion du système de cache

*/

	'cache' => array(
		'useCache' => False,
		'duration' => 7200,
		'allowFor' => array(
				'welcome'
			)
		),



/*
	* logs
	Configuration de l'outil de log

*/	

	'logs'	=> array(
		'useLogs'	=> False,
		'format'	=> '%ip ---- %date ---- %request'
		),



/*
	* cron
	Gestion de cron

*/	

	'cron'	=> array(
		'useCron'	=> False,
		'actions'	=> ''
		),



/*
	* routes
	Configuration des routes des class

*/	
		
	'roads'	=> array(
		'useRoads'	=> False,
		'rules'	=> array(
			'accueil'		=> 'welcome'
		)	
	)
);



/*
	* variables
	Définition des variables système

*/

define("SHOW_STATS_CACHE", False);