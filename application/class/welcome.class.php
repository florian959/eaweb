<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 27 juin 2015

/*
	* class ~ welcome.class.php
	Class de la page d'accueil
	Chargement de la vue views ~ welcome-message.php

*/

if(!defined("BASE_DIR")){die("403");}

Class welcome extends view
{
	public function __construct()
	{
		vars::set("syslang", "fr");
		lib::load("test");

		//lib_test::hello();

	}
	static public function main()
	{
		view::load("welcome-message");
	}
}
