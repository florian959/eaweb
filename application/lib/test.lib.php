<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 27 juin 2015

/*
	* lib ~ test.php
	Librairie de démonstration

*/

if(!defined("BASE_DIR")){die("403");}

Class lib_test
{
	public function __construct()
	{

	}
	static public function hello()
	{
		echo "Hello, Dolly!";
	}
}