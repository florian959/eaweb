<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 22 juin 2015

/*
	* expressions ~ langage.php
	Définit les traductions

*/

$langage = array(
	'settings' => array(
			"state"		=> "on" // On/Off
		),
	

	'installe_avec_succes'	=> array(
			'fr'	=> 	'Installé avec succés!',
			'en'	=>	'Successfully installed!'
		),

	'phrase_intro'	=> array(
			'fr'	=> 'Mon site',
			'en'	=> 'My website'
		)
	);
