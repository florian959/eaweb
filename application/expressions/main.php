<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 27 juin 2015

/*
	* expressions ~ main.php
	Définitions les expressions {{clé}}
	Principales expressions

*/

$alias = array(
	'settings' => array(
			"state"		=> "on"//  On/Off
		),
	
	'values'   => array(
			"{{time}}" 					=> time(),
			"{{date}}" 					=> date('d-m-Y'),
			"{{heure}}" 				=> date('H:i:s'),
			"{{cacheDurationMins}}"		=> functions::getConfig()['cache']['duration']/60,
			"{{elapsed_time}}"			=> round((microtime(true)-$_SESSION['BEGIN_TIME']), 3)
		), 
	);

include_once("smileys.php");