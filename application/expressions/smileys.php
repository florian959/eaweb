<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 26 juin 2015

/*
	* expressions ~ smileys.php
	Définitions des smileys :code:
	Principales expressions

*/

$alias['values'][':smile:'] = "<img src='".str_replace('/index.php', '', app::url()).$this->paths['master']['ressources']."/smileys/smile.png' width='25px' height='25px' alt='smile' style='vertical-align: middle;'>";
$alias['values'][':sad:']	= "<img src='".str_replace('/index.php', '', app::url()).$this->paths['master']['ressources']."/smileys/sad.png' width='25px' height='25px' alt='sad' style='vertical-align: middle;'>";
$alias['values'][':love:']	= "<img src='".str_replace('/index.php', '', app::url()).$this->paths['master']['ressources']."/smileys/love.png' width='25px' height='25px' alt='love' style='vertical-align: middle;'>";