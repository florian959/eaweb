<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 20 juin 2015

/*
	* LOGS
	Ecriture des logs dans les fichiers

*/

Class LOGS
{
	public $format;
	public $request;
	public $ip;
	public $date;
	public $path;
	public $file;

	public function __construct()
	{

	}
	public function writeLogs($path, $format, $request, $ip)
	{
		$this->format = $format;
		$this->request = $request;
		$this->ip = $ip;
		$this->date = date("d-m-Y H:i:s");
		$this->path = $path;

		if(preg_match("#%ip#", $this->format))
		{
			$this->format = str_replace("%ip", $this->ip, $this->format);
		}

		if(preg_match("#%date#", $this->format))
		{
			$this->format = str_replace("%date", $this->date, $this->format);
		}

		if(preg_match("#%request#", $this->format))
		{
			$this->format = str_replace("%request", $this->request, $this->format);
		}

		if(!is_dir(".".$this->path))
		{
			mkdir(".".$this->path);
		}

		$this->file = @fopen(".".$this->path."/".date("d-m-Y.H").".logs", "a+");
						
		@fwrite($this->file, $this->format."\n");
						
		@fclose($this->file);
		
	}
}
$logs = new LOGS();