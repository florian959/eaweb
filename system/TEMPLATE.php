<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 20 juin 2015

/*
	* TEMPLATE
	Affichage du contenu de la page

*/

Class TEMPLATE
{

public $controler;
public $class;
public $paths;
public $config;
public $isPreclass;

	public function __construct()
	{
		$this->class = htmlspecialchars($_SESSION['_PAGE_REQUEST_']);
		$this->controler = htmlspecialchars($_SESSION['_PAGE_REQUEST_']);
	}

	public function LOAD($paths,$config)
	{
		$this->paths = $paths;
		$this->config = $config;

		if(file_exists(".".$this->paths['application']['class']."/".$this->class.".class.php"))
		{
			$this->class = ".".$this->paths['application']['class']."/".$this->class.".class.php";
		}
		else
		{
			$this->class = ".".$this->paths['application']['common']."/class/default.class.php";
			$this->isPreclass = True;
		}
	}

	public function content()
	{
		$view = new view();
		$class = new classer();

		if(file_exists(".".$this->paths['application']['common']."/class/common.class.php"))
		{
			include_once(".".$this->paths['application']['common']."/class/common.class.php");

			if(class_exists('CLASScommon'))
			{
				new CLASScommon();
				CLASScommon::main();
			}
			else
			{

				throw new Exception("Error: Class CLASScommon not found", 1);
				
			}
		}
		else
		{

			throw new Exception("Error: File not found (common.class.php)", 1);
			
		}

		if(!empty($this->class))
		{
			include_once($this->class);
			if(empty($this->isPreclass))
			{
				$class_ = htmlspecialchars($_SESSION['_PAGE_REQUEST_']);
				$class_ = str_replace('/', '_', $class_);
				if(class_exists($class_))
				{
					$class = new $class_();
					$class::main();
				}
				else
				{
					throw new Exception("Error: Class '".$class_."' not found", 1);
					
				}
				
			}
			else
			{
				if(class_exists('CLASSdefault'))
				{
					new CLASSdefault();
					CLASSdefault::main();
				}
				else
				{
					throw new Exception("Error: Class CLASSdefault not found", 1);
					
				}
			}
		}
		
	}

	public function __destruct()
	{
		
	}
}
$template = new TEMPLATE();