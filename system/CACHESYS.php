<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 22 juin 2015

/*
	* CACHESYS
	Affiche le cache, enregistre le cache
	Affiche les statistiques du cache

*/

Class CACHESYS
{
	public function __construct()
	{

	}
	public function LOAD($config = array('useCache' => True),$paths)
	{
		if($config['useCache'] == True AND isset($_SESSION['_PAGE_REQUEST_']) AND !empty($_SESSION['_PAGE_REQUEST_']))
		{

			if(in_array($_SESSION['_PAGE_REQUEST_'], $config['allowFor']))
			{

				if(!is_dir(".".$paths['application']['cache']))
				{
					mkdir(".".$paths['application']['cache']);
				}

				if(file_exists(".".$paths['application']['cache']."/".htmlspecialchars($_SESSION['_PAGE_REQUEST_']).".cache"))
				{

					$filemtime = time()-filemtime(".".$paths['application']['cache']."/".htmlspecialchars($_SESSION['_PAGE_REQUEST_']).".cache");

					if($filemtime > $config['duration'])
					{

						unlink(".".$paths['application']['cache']."/".htmlspecialchars($_SESSION['_PAGE_REQUEST_']).".cache");
						
						header("Refresh: 0; url=".$_SERVER['PHP_SELF']);

					}

					if(file_exists(".".$paths['application']['cache']."/".htmlspecialchars($_SESSION['_PAGE_REQUEST_']).".cache"))
					{
						echo file_get_contents(".".$paths['application']['cache']."/".htmlspecialchars($_SESSION['_PAGE_REQUEST_']).".cache");

						if(defined('SHOW_STATS_CACHE') AND SHOW_STATS_CACHE == True)
						{
							echo "<pre>CACHE STATS, TIME: ".round((microtime(true)-$_SESSION['BEGIN_TIME']), 3)." seconds.</pre>";
						}
						
						exit();
					}

				}

			}

			
		}
	}

	public function writeCache($config, $paths, $content)
	{
		if($config['cache']['useCache'] == True AND isset($_SESSION['_PAGE_REQUEST_']) AND !empty($_SESSION['_PAGE_REQUEST_']) AND in_array($_SESSION['_PAGE_REQUEST_'], $config['cache']['allowFor']))
		{
			if(!is_dir(".".$paths['application']['cache']))
			{
				mkdir(".".$paths['application']['cache']);
			}
			
			if(!file_exists(".".$paths['application']['cache']."/".htmlspecialchars($_SESSION['_PAGE_REQUEST_'].".cache")))
			{

				if(!@fopen(".".$paths['application']['cache']."/".htmlspecialchars($_SESSION['_PAGE_REQUEST_']).".cache", "r"))
				{
					$explode = explode("/", htmlspecialchars($_SESSION['_PAGE_REQUEST_']));

					if(!empty($explode))
					{
						$del = count($explode);
						unset($explode[$del]);
					}

					foreach($explode as $exploded)
					{
						if(!empty($explode) AND $del > 1 AND !is_dir(".".$paths['application']['cache']."/".htmlspecialchars($exploded)))
						{
							mkdir(".".$paths['application']['cache']."/".htmlspecialchars($exploded));
							chmod(".".$paths['application']['cache']."/".htmlspecialchars($exploded), 0777);
						}
					}
				}

				$cache = @fopen(".".$paths['application']['cache']."/".htmlspecialchars($_SESSION['_PAGE_REQUEST_']).".cache", "a+");
						
				@fwrite($cache, $content);
						
				@fclose($cache);
			}
		}
	}

	public function clearCache()
	{
		if(is_dir(".".$paths['application']['cache']))
		{
			rmdir(".".$paths['application']['cache']);
		}
	}
}
$cachesys = new CACHESYS();