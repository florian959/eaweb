<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifi� le: 27 juin 2015

/*
	* LOAD
	Module principal du noyau
	G�n�ration de la requ�te

*/

Class LOAD
{

public $request;
public $paths;
public $config;
public $urlSeparator;
public $urlValueSeparator;
public $ip;

	public function __construct()
	{
		$this->request = $_SERVER['PHP_SELF'];
		$this->urlSeparator = array();
		$this->ip = $_SERVER['REMOTE_ADDR'];

		$_SESSION['BEGIN_TIME'] = microtime(true);
		define("BASE_DIR", True);
	}
	public function LOAD($paths,$config)
	{
		$this->paths = $paths;
		$this->config = $config;

		if($this->config['mode'] == 'dev')
		{
			ini_set('display_errors', 1);

			error_reporting(E_ALL);
		}
		else
		{
			ini_set('display_errors', 0);
		}
		$this->request = str_replace($paths['base-url'],"",$this->request);
		if($this->request == '/index.php' OR $this->request == 'index.php' OR $this->request == 'index.php/' OR $this->request == '/index.php/' OR $this->request == '')
		{
			$this->request = 'welcome';
		}
		else
		{
			$this->request = str_replace("/index.php/","",$this->request);
		}
		if(preg_match("#:#", $this->request))
	    {
		      $this->urlSeparator = explode(":",$this->request);
		      $intCount = 0;
			foreach($this->urlSeparator as $urlSeparator)
			{
			  if($intCount > 0)
			  {
			    $this->request = str_replace(":".$this->urlSeparator[$intCount], "", $this->request);
			    $_SESSION["_SEPARATOR_".$intCount] = $this->urlSeparator[$intCount]; 
			  }
			  $intCount = $intCount+1;
			}  
	    }
		foreach($this->config['plugins'] as $key => $value)
		{
			if($value['Load'] == 'onstart' AND file_exists(".".$this->paths['system']."/plugins/".$value['file']))
			{
				include_once(".".$this->paths['system']."/plugins/".$value['file']);
				$class_ = new $key($this->paths, $this->config, $this->request);
				$class_->main();
			}
		}
		$_SESSION['_PAGE_REQUEST_'] = $this->request;
		include("ROADS.php");
		$ROADS->LOAD($this->config['roads']);
		
		if($this->config['logs']['useLogs'] == True)
		{
			include("LOGS.php");
			$logs->writeLogs($this->paths['application']['logs'], $this->config['logs']['format'], $this->request, $this->ip);
		}
		
	}
	public function __destruct()
	{

	}
}

include_once("FUNCTIONS.php");
include_once("CRON.php");
$load = new LOAD();