<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 27 juin 2015

/*
	* ALIAS
	Génére le rendu de la page et l'affiche
	Transforme les codes {{clé}} en valeur

*/

Class ALIAS
{

public $paths;
public $config;
public $content;
public $lang;

	public function __construct()
	{

	}
	public function LOAD($paths,$config)
	{
		$this->paths = $paths;
		$this->config = $config;
		require_once("./".$this->paths['application']['alias']."/main.php");
		require_once("./".$this->paths['application']['alias']."/langage.php");
		require_once("./".$this->paths['application']['main']."/configuration.php");		

		$this->content = ob_get_contents(); 
		if($alias['settings']['state'] == 'on')
		{
			foreach($alias['values'] as $key => $value)
			{
				if(preg_match("#".$key."#", $this->content))
				{
					$this->content = str_replace(''.$key.'', $value, $this->content); 
				}
			}
		}	

		foreach($_SESSION['_VAR_'] as $key => $value)
		{
			if(preg_match("#-var ".$key." var-#", $this->content))
			{
				$this->content = str_replace('-var '.$key.' var-', $value, $this->content); 
			}
		}

		if($langage['settings']['state'] == 'on')
		{
			//echo vars::set('syslang');
			if(!vars::set('syslang'))
			{
				$this->lang = $this->config['langages'][0];
			}
			else
			{
				$this->lang = vars::set('syslang');
			}
			if(isset($_GET['lang']) AND in_array($_GET['lang'], $config['langages']))
			{
				$this->lang = htmlspecialchars($_GET['lang']);
			}
			foreach($langage as $key => $value)
			{
				if($key != 'settings')
				{
					if(preg_match("#".$key."#", $this->content))
					{
						$this->content = str_replace('{{'.$key.'}}', $langage[$key][$this->lang], $this->content); 
					}
					if(preg_match("#-lang ".$key." lang-#", $this->content))
					{
						$this->content = str_replace('-lang '.$key.' lang-', $langage[$key][$this->lang], $this->content); 
					}
				}
			}
		}
	}
	public function RENDER()
	{
		ob_end_clean(); 
		$ShowContent = True;
		foreach($this->config['plugins'] as $key => $value)
		{
			if($value['Load'] == 'onfinish' AND file_exists(".".$this->paths['system']."/plugins/".$value['file']))
			{
				include_once(".".$this->paths['system']."/plugins/".$value['file']);
				$class_ = new $key($this->paths, $this->config, $this->content);
				$class_->main();
				if(isset($value['ShowContent']) AND $value['ShowContent'] == False)
				{
					$ShowContent = False;
				}
			}
		}
		if($ShowContent == True)
		{
			echo $this->content;
		}

		include_once("CACHESYS.php");

		$cachesys = new CACHESYS();
		
		$cachesys->writeCache($this->config, $this->paths, $this->content);
		
	}
	public function __destruct()
	{

	}
}
$alias = new ALIAS();