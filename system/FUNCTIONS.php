<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 20 juin 2015

/*
	* FUNCTIONS
	Librairie de fonctions

*/
Class vars
{
	public function __construct()
	{

	}
	static public function set($name,$value = '')
	{
		if(!empty($value))
		{
			$_SESSION['_VAR_'][$name] = $value;
		}
		else
		{
			if(isset($_SESSION['_VAR_'][$name]))
			{
				return $_SESSION['_VAR_'][$name];
			}
			return 0;
		}
	}
}
Class app
{
	public function __construct()
	{

	}
	static public function url()
	{
		if(functions::getConfig()['url']['useIndex'] == True)
		{
			return functions::getConfig()['short-url'].'index.php';
		}
		else
		{
			return functions::getConfig()['short-url'];
		}
	}
	static public function css()
	{
		return functions::getConfig()['css-folder'];
	}
	static public function js()
	{
		return functions::getConfig()['js-folder'];
	}
	static public function langages()
	{
		return $_SESSION['_CONFIG_']['langages'];
	}
}
Class functions
{
	public function __construct()
	{

	}
	static public function getConfig()
	{
		return $_SESSION['_CONFIG_'];
	}
	static public function getPath()
	{
		return $_SESSION['_PATHS_'];
	}
	static public function loop($function, $nbr)
	{
		for ($i=0; $i < $nbr; $i++) { 
			echo $function;
		}
	}
	static public function separatorId($key,$rm = 0)
	{    
	  if(isset($_SESSION["_SEPARATOR_" . $key]))
	  {
		$return = $_SESSION["_SEPARATOR_" . $key];
		if($rm == 1)
		{
			unset($_SESSION["_SEPARATOR_" . $key]);
		} 
		return $return;
	  }
	}
	static public function receive($name,$rm = 0) {
	  if(isset($_SESSION["_VAR_".$name]))
	  {
	    $value =  $_SESSION["_VAR_".$name];
	    if($rm == 1)
		{
	    	unset($_SESSION["_VAR_".$name]);
		} 
	    return $value;
	  }
	}
	static public function defineLang($lang = '')
	{
		if(!empty($lang))
		{
			vars::set("syslang",$lang);
		}
		else
		{
			vars::set("syslang",$lang);
		}
		vars::set("syslang",$lang);
	}
	static public function getLang()
	{
		if(!empty($_SESSION['_VAR_syslang']))
		{
			return $_SESSION['_VAR_syslang'];
		}
		else
		{
			return 0;
		}
	}
	static public function wrt($text = '#empty text')
	{
		print($text);
	}
}