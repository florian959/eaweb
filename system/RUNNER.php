<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 20 juin 2015

/*
	* RUNNER
	Charge les vues / class

*/

Class RUNNER
{

public $paths;
public $config;

	public function __construct()
	{
		
	}

	public function LOAD($paths,$config)
	{
		$this->paths = $paths;
		$this->config = $config;
		$_SESSION['_PATHS_'] = $this->paths;
		$_SESSION['_CONFIG_'] = $this->config;
	}

	public function __destruct()
	{
		
	}
}

Class view
{
	public function __construct($view = '')
	{
		if(!empty($view))
		{
			$this->load($view);
		}
	}

	static public function load($view)
	{
		if(file_exists(".".$_SESSION['_PATHS_']['application']['views']."/".$view.".php"))
		{
			include_once(".".$_SESSION['_PATHS_']['application']['views']."/".$view.".php");
		}
		else
		{
			throw new Exception("Error: view '".htmlspecialchars($view)."' not found", 1);
		}
	}

	public function __destruct()
	{
		
	}
}

Class classer
{
	public function __construct()
	{
		
	}

	static public function load($name)
	{
		if(file_exists("./".$_SESSION['_PATHS_']['application']['class']."/".$name.".class.php"))
		{
			include_once("./".$_SESSION['_PATHS_']['application']['class']."/".$name.".class.php");
		}
		else
		{
			throw new Exception("Error: Class '".htmlspecialchars($name)."' not found", 1);
		}
	}

	public function __destruct()
	{
		
	}
}

Class lib
{
	public function __construct()
	{
		
	}

	static public function load($name)
	{
		if(file_exists("./".$_SESSION['_PATHS_']['application']['lib']."/".$name.".lib.php"))
		{
			include_once("./".$_SESSION['_PATHS_']['application']['lib']."/".$name.".lib.php");
		}
		else
		{
			throw new Exception("Error: Lib '".htmlspecialchars($name)."' not found", 1);
		}
	}

	public function __destruct()
	{
		
	}
}
$runner = new RUNNER();