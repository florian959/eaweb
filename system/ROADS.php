<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 20 juin 2015

/*
	* ROADS
	Modification des requêtes en fonction des
	paramètres indiqués

*/

Class ROADS
{
	public $roads;
	public $config;
	public function __construct()
	{
		
	}
	public function LOAD($config)
	{
		$this->config = $config;
		if($this->config['useRoads'] == True AND !empty($this->config['rules']))
		{
					foreach($this->config['rules'] as $key => $value)
					{
						if($_SESSION['_PAGE_REQUEST_'] == $key)
						{
							$_SESSION['_PAGE_REQUEST_'] = $value;						
						}
					}
		}
	}
}
$ROADS = new ROADS();