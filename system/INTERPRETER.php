<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 20 juin 2015

/*
	* INTREPRETER
	Compile le code wb

*/

Class INTERPRETER
{
	public $rand;
	public $compiled;
	public $temp;

	public function __construct()
	{

	}
	public function compile($code)
	{
		$this->rand = rand(10000,99999);

		$this->temp = fopen("./system/temp/".$this->rand.".php", "w");

		$this->compiled = $code;

			$this->compiled = str_replace('[ ', '(', $this->compiled);
			$this->compiled = str_replace(' ]', ')', $this->compiled);
			
			$this->compiled = str_replace('@var ', '$', $this->compiled);
			$this->compiled = str_replace('`', '"', $this->compiled);
			$this->compiled = str_replace('loop', 'while', $this->compiled);
			$this->compiled = str_replace('!comment ', '//', $this->compiled);
			$this->compiled = str_replace('!com', '/*', $this->compiled);
			$this->compiled = str_replace('com!', '*/', $this->compiled);
			$this->compiled = str_replace('func', 'function', $this->compiled);
			$this->compiled = str_replace('Block', 'Class', $this->compiled);
			$this->compiled = str_replace('@inst', 'new', $this->compiled);
			$this->compiled = str_replace(' obj: ', '->', $this->compiled);
			$this->compiled = str_replace('with%', 'AND', $this->compiled);
			$this->compiled = str_replace('array%', 'foreach', $this->compiled);
						
			fwrite($this->temp, "<?php ".$this->compiled);
						
			fclose($this->temp);

		include("./system/temp/".$this->rand.".php");

		unlink("./system/temp/".$this->rand.".php");
	}

}
$interpreter = new INTERPRETER();