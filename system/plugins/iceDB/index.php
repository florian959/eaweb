<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 28 juin 2015

/*
	* plugins ~ iceDB / index.php
	Plugin de base de données iceDB

*/

Class iceDB
{
	public function __construct($paths, $config, $request)
	{
		
	}

	static public function query($req)
	{
		if(preg_match("#^SELECT #", $req))
		{
			$req = str_replace("SELECT ", "", $req);

			if(preg_match("#^ALL DATAS FROM #", $req))
			{
				$req = str_replace("ALL DATAS FROM ", "", $req);
				if(is_dir(".".$_SESSION['_PATHS_']['system']."/plugins/iceDB/STORAGE"))
				{
					if(file_exists(".".$_SESSION['_PATHS_']['system']."/plugins/iceDB/STORAGE/".$req))
					{
						$resultat = fopen(".".$_SESSION['_PATHS_']['system']."/plugins/iceDB/STORAGE/".$req, 'r');
						$resultats = array();
						if($resultat)
						{
							while(!feof($resultat))
							{
								$ligne = fgets($resultat);
								if(!preg_match("#^---#", $ligne))
								{
									$explode = explode('		|		', $ligne);
									$resultats[] = $explode;
								}
							}

							fclose($resultat);


							return $resultats;
						}
						else
						{
							return 0;
						}
					}
					else
					{
						throw new Exception("Error: iceDB, table '".htmlspecialchars($req)."' not found", 1);
					}
				}
			}
		}
		if(preg_match("#^INSERT #", $req))
		{
			
		}
	}

	static public function main()
	{
		
	}
}