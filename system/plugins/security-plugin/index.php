<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 23 juin 2015

/*
	* plugins ~ security-plugin / index.php
	Plugin de sécurité

*/

Class security
{

	public $paths;
	public $config;
	public $request;
	public $headers;
	public $get;
	public $post;
	public $rules;
	public $ip;
	public $error_message;

	public function __construct($paths, $config, $request)
	{
		$this->paths	=	$paths;
		$this->config	=	$config;
		$this->request	= 	$request;
		$this->ip = $_SERVER['REMOTE_ADDR'];
		$this->rules	=	array(
				'get'	=> array(
								file_get_contents('.'.$this->paths['system'].'/plugins/security-plugin/rules/get.rule.txt')
							),
				'post'	=> array(
								file_get_contents('.'.$this->paths['system'].'/plugins/security-plugin/rules/post.rule.txt'),
								file_get_contents('.'.$this->paths['system'].'/plugins/security-plugin/rules/termes.rule.txt')
							),
				'ip'	=> array(
								file_get_contents('.'.$this->paths['system'].'/plugins/security-plugin/rules/ip.rule.txt')
							)
			);
		$this->error_message = "<div style='padding: 10px; background-color: #ff0000; color: #ffffff; font-size: 25px;'><pre>Requete bloquée.</pre></div>";
	}

	public function checkIP()
	{
			foreach($this->rules['ip'] as $key2 => $value2)
			{
				if(!empty($value2) AND preg_match("#".$this->rules['ip'][$key2]."#", $this->ip))
				{
					die($this->error_message);
				}
			}
	}

	public function url()
	{
		
	}

	public function get()
	{
		$this->get = $_GET;

		foreach($this->get as $key => $value)
		{
			foreach($this->rules['get'] as $key2 => $value2)
			{
				if(!empty($value2) AND preg_match("#".$this->rules['get'][$key2]."#", $value))
				{
					die($this->error_message);
				}
			}
		}
	}

	public function post()
	{
		$this->post = $_POST;

		foreach($this->post as $key => $value)
		{
			foreach($this->rules['post'] as $key2 => $value2)
			{
				if(!empty($value2) AND preg_match("#".$this->rules['post'][$key2]."#", $value))
				{
					die($this->error_message);
				}
			}
				$_POST[$key]	=	htmlspecialchars($value);
		}
	}

	public function headers()
	{
		$this->headers = apache_response_headers();
	}

	public function main()
	{
		$this->checkIP();
		$this->headers();
		$this->post();
		$this->url();
		$this->get();
	}
}