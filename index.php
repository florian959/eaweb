<?php
//  __________      ___       __    ______   // 
//  ___  ____/_____ __ |     / /_______  /_  // 
//  __  __/  _  __ `/_ | /| / /_  _ \_  __ \ // 
//  _  /___  / /_/ /__ |/ |/ / /  __/  /_/ / // 
//  /_____/  \__,_/ ____/|__/  \___//_.___/  // 

// Eaweb, cadriciel pour applicatons web en php
// Modifié le: 22 juin 2015

/*
	* index.php
	Bienvenue sur l'index :-)

*/
                                        
// UTF-8
header('content-type: text/html; charset=utf-8');

// Buffering
ob_start();

// Configuration des dossiers
$paths = array(
	'system'      => '/system',
	'application' => array(
			'main'       => '/application',
			'views'      => '/application/views',
			'class'		 => '/application/class',
			'alias'		 => '/application/expressions',
			'common'	 => '/application/common',
			'lib'		 => '/application/lib',
			'cache'		 => '/application/cache',
			'logs'		 => '/application/logs'
		),
	'master'      => array(
			'main'       => '/static',
			'ressources' => '/static/ressources',
			'librairies' => '/static/librairies',
			'template'   => '/static/template'
		),
	'base-url'    => str_replace("/index.php","",$_SERVER['SCRIPT_NAME'])
);

	require(dirname(__FILE__).$paths['application']['main'].'/configuration.php');

	session_start();

try
{

	require(dirname(__FILE__).$paths['system'].'/LOAD.php');
		$load->LOAD($paths,$config);

	require(dirname(__FILE__).$paths['system'].'/CACHESYS.php');
		$cachesys->LOAD($config['cache'], $paths);	

	require_once(".".$paths['system']."/RUNNER.php");
		$runner->LOAD($paths,$config);
		
	require_once(".".$paths['system']."/TEMPLATE.php");
		$template->LOAD($paths,$config);
	
	include_once("./".$config['template']);
		
	require(dirname(__FILE__).$paths['system'].'/ALIAS.php');
		$alias->LOAD($paths,$config);
		$alias->RENDER();

}

Catch(Exception $e)
{
	echo "<div style='padding: 10px; background-color: #ff0000; color: #ffffff; font-size: 25px;'><pre>".$e->getMessage()."</pre></div>";
}
