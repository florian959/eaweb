# Eaweb
EaWeb est un mini cadriciel facilitant le développement de petites applications web.

## Installation
Décompressez l'archive dans le dossier de votre serveur, ouvrez votre navigateur à l'adresse de votre serveur où est installé eaweb /index.php.
<br />
Si "Installé avec succés!" s'affiche, alors c'est gagné :)

## Première page
Une page est composée d'une class, nommée nom.class.php où 'nom' sera le lien pour y accéder. Ex: la page contact de mon site a pour class contact.class.php et est accessible à l'adresse ladressedemonsite.com/index.php/contact
<br />
Le contenu d'une class est le suivant:
<br />
